import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'

Vue.config.productionTip = false

let GlobalData = new Vue({
  data: {
    $rawComments: []
  }
});

Vue.mixin({
  methods: {
    $_rawComments(){
      return GlobalData.$data.$rawComments
    },
    $_setRawComments(newRowComments){
      GlobalData.$data.$rawComments = newRowComments;
    }
  },
  computed: {
    $rawComments: {
      get: function () { return GlobalData.$data.$rawComments },
      set: function (newRowComments) { GlobalData.$data.$rawComments = newRowComments; }
    }
  }
});

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
