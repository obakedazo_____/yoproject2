const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: [
    'vuetify'
  ],
  pluginOptions: {
    electronBuilder: {
      builderOptions: {
        // options placed here will be merged with default configuration and passed to electron-builder
        linux: {
          target: ["AppImage"],
          icon: "build/icons/"
        },
        win: {
          target: ["portable"],
          icon: "build/icons/icon.ico"
        }
      }
    }
  }
})
