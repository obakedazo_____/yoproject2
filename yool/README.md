# yool

memo

npm run electron:serve
npm run electron:build
npm run electron:generate-icons
license-checker --production --csv --out ./report/licernses.csv --unknown

## リリースノート

### 2023.04.29 Ver1.0.1

- チーム戦に対応、3vs3vs2とか変則的なものは非対応
- 参加者・Note追加ダイアログで、外側をクリックした時にダイアログが閉じないようにしました。これによりリストを外側クリックで閉じた時一緒にダイアログが閉じることがなくなりました。

### 2023.06.14 Ver1.1.0

- 並び替えに対応
- 
### 2023.06.15 Ver1.1.1

- /join コマンド: 参加希望
- /bye コマンド: 次抜ける
- 
### 2023.06.25 Ver1.1.2

- /cheese コマンド: 🧀
- Ownerがずれるバグの修正

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### License

This project is licensed under the MIT License, see the LICENSE file for details

