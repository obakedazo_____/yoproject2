import Vue from 'vue'
import Router from 'vue-router'
import Hello from './components/HelloWorld.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'hello',
      component: Hello
    },
    {
      path: '/participant',
      name: 'participant',
      component: () => import('./views/Participant.vue')
    },
    {
      path: '/home',
      name: 'home',
      component: () => import('./views/Home.vue')
    },
    {
      path: '/comment/:raw*',
      name: 'comment',
      props: true,
      component: () => import('./views/Comment.vue')
    }
  ]
})