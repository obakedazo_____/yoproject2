## YouTubeAPI有効化

[YouTubeAPI参考](https://qiita.com/iroiro_bot/items/ad0f3901a2336fe48e8f)

### キー作成

1. アクセス\
    https://console.cloud.google.com/apis/credentials

1. キー作成、コピー
    YouTubeAPI参考をみて

### LiveChartID取得

1. これ使う\
    https://developers.google.com/youtube/v3/docs/videos/list?hl=ja&apix_params=%7B%22part%22%3A%5B%22liveStreamingDetails%22%5D%2C%22id%22%3A%5B%22BCOb8cS352I%22%5D%7D

    ex:
    ```
    curl 'https://www.googleapis.com/youtube/v3/videos?part=liveStreamingDetails&id=CVUphulYMps&key=AIzaSyARx_t9oc9ILXrLsQGpOExLupHWg2rDtGQ'
    ```

1. result の　activeLiveChatId使う
    ```
    {
        "kind": "youtube#videoListResponse",
        "etag": "GFfL8DR3le65w_D-C6dKOszOYGU",
        "items": [
            {
            "kind": "youtube#video",
            "etag": "Vz2wyW5KdiYXAwQKN9ktpgr0E4g",
            "id": "BCOb8cS352I",
            "liveStreamingDetails": {
                "actualStartTime": "2022-10-30T06:03:20Z",
                "scheduledStartTime": "2022-10-30T06:02:28Z",
                "concurrentViewers": "2",
                "activeLiveChatId": "Cg0KC0JDT2I4Y1MzNTJJKicKGFVDSEItd21TUzM4VmYzTnU3VUpBS29IdxILQkNPYjhjUzM1Mkk"
            }
            }
        ],
        "pageInfo": {
            "totalResults": 1,
            "resultsPerPage": 1
        }
    }
    ```

### chat取得

1. ↑で取得したLiveChatIDつかってコメント取得

    https://developers.google.com/youtube/v3/live/docs/liveChatMessages/list?apix_params=%7B%22liveChatId%22%3A%22BCOb8cS352I%22%2C%22part%22%3A%5B%22id%22%5D%7D

    request
    ```
    curl 'https://youtube.googleapis.com/youtube/v3/liveChat/messages?liveChatId=Cg0KC0NWVXBodWxZTXBzKicKGFVDRHNieTFZeXJIazZuRy1LT1JLUHJWQRILQ1ZVcGh1bFlNcHM&part=authorDetails,snippet&hl=ja&key=AIzaSyARx_t9oc9ILXrLsQGpOExLupHWg2rDtGQ'
    ```
    
    <details>
    <summary>response</summary>

    ```
    "kind": "youtube#liveChatMessageListResponse",
    "etag": "XspOrbJf-J4t6g48IjqQDyVUHcU",
    "pollingIntervalMillis": 1921,
    "pageInfo": {
        "totalResults": 9,
        "resultsPerPage": 9
    },
    "nextPageToken": "GMWRqbGoh_sCIO3I7MCxh_sC",
    "items": [
        {
        "kind": "youtube#liveChatMessage",
        "etag": "pa2h0dAKShe_4hW3Ltmv4pnQ74M",
        "id": "LCC.CjgKDQoLQkNPYjhjUzM1MkkqJwoYVUNIQi13bVNTMzhWZjNOdTdVSkFLb0h3EgtCQ09iOGNTMzUySRI6ChpDSnpKNjUta2hfc0NGUllOclFZZFRDY0hpQRIcQ09qNXo1MmtoX3NDRlRsT0R3SWR6YVlGSlEtMA",
        "snippet": {
            "type": "textMessageEvent",
            "liveChatId": "Cg0KC0JDT2I4Y1MzNTJJKicKGFVDSEItd21TUzM4VmYzTnU3VUpBS29IdxILQkNPYjhjUzM1Mkk",
            "authorChannelId": "UCRNn0QpuRX8RHqTX_BmvEMg",
            "publishedAt": "2022-10-30T06:04:36.382131+00:00",
            "hasDisplayContent": true,
            "displayMessage": "あ",
            "textMessageDetails": {
            "messageText": "あ"
            }
        },
        "authorDetails": {
            "channelId": "UCRNn0QpuRX8RHqTX_BmvEMg",
            "channelUrl": "http://www.youtube.com/channel/UCRNn0QpuRX8RHqTX_BmvEMg",
            "displayName": "さぶりみなるおばけ",
            "profileImageUrl": "https://yt3.ggpht.com/IKZ5my1_qxtCJ2_7pwWMSh2ldSvj0TTYr41o7J-EL8VeLT7KKMPFE5jLQM8lwOh0yLcVcTjgkg=s88-c-k-c0x00ffffff-no-rj",
            "isVerified": false,
            "isChatOwner": false,
            "isChatSponsor": false,
            "isChatModerator": false
        }
        },
        {
        "kind": "youtube#liveChatMessage",
        "etag": "Vi9aWT1FnRnmz25PzoMq8ytWO8I",
        "id": "LCC.CjgKDQoLQkNPYjhjUzM1MkkqJwoYVUNIQi13bVNTMzhWZjNOdTdVSkFLb0h3EgtCQ09iOGNTMzUySRI6ChpDTFB1aDZXa2hfc0NGVUlWZlFvZFZ6QUtpURIcQ09qNXo1MmtoX3NDRlRsT0R3SWR6YVlGSlEtMQ",
        "snippet": {
            "type": "textMessageEvent",
            "liveChatId": "Cg0KC0JDT2I4Y1MzNTJJKicKGFVDSEItd21TUzM4VmYzTnU3VUpBS29IdxILQkNPYjhjUzM1Mkk",
            "authorChannelId": "UCRNn0QpuRX8RHqTX_BmvEMg",
            "publishedAt": "2022-10-30T06:04:47.327469+00:00",
            "hasDisplayContent": true,
            "displayMessage": "い",
            "textMessageDetails": {
            "messageText": "い"
            }
        },
        "authorDetails": {
            "channelId": "UCRNn0QpuRX8RHqTX_BmvEMg",
            "channelUrl": "http://www.youtube.com/channel/UCRNn0QpuRX8RHqTX_BmvEMg",
            "displayName": "さぶりみなるおばけ",
            "profileImageUrl": "https://yt3.ggpht.com/IKZ5my1_qxtCJ2_7pwWMSh2ldSvj0TTYr41o7J-EL8VeLT7KKMPFE5jLQM8lwOh0yLcVcTjgkg=s88-c-k-c0x00ffffff-no-rj",
            "isVerified": false,
            "isChatOwner": false,
            "isChatSponsor": false,
            "isChatModerator": false
        }
        },
        {
        "kind": "youtube#liveChatMessage",
        "etag": "LR2QFL7z4FkMmoRn1TjhAoC8Feg",
        "id": "LCC.CjgKDQoLQkNPYjhjUzM1MkkqJwoYVUNIQi13bVNTMzhWZjNOdTdVSkFLb0h3EgtCQ09iOGNTMzUySRI6ChpDSkRlM0t1bGhfc0NGVVVhZlFvZFIzd0hpURIcQ01URGlwLWxoX3NDRllrTllBb2RwVklQREEtMA",
        "snippet": {
            "type": "textMessageEvent",
            "liveChatId": "Cg0KC0JDT2I4Y1MzNTJJKicKGFVDSEItd21TUzM4VmYzTnU3VUpBS29IdxILQkNPYjhjUzM1Mkk",
            "authorChannelId": "UCYplhZebjDAm_69peY5YR2g",
            "publishedAt": "2022-10-30T06:09:29.72953+00:00",
            "hasDisplayContent": true,
            "displayMessage": "こん🧀w",
            "textMessageDetails": {
            "messageText": "こん🧀w"
            }
        },
        "authorDetails": {
            "channelId": "UCYplhZebjDAm_69peY5YR2g",
            "channelUrl": "http://www.youtube.com/channel/UCYplhZebjDAm_69peY5YR2g",
            "displayName": "ゆあいだ。",
            "profileImageUrl": "https://yt3.ggpht.com/7slUuc7csEwF4WjF2rG_IBMNVm9VkdLgjtzMEP4iNFFYffdAEm4JW7qVwrfb8JCMJNwdTTfAeA=s88-c-k-c0x00ffffff-no-rj",
            "isVerified": false,
            "isChatOwner": false,
            "isChatSponsor": false,
            "isChatModerator": false
        }
        },
        {
        "kind": "youtube#liveChatMessage",
        "etag": "iHlbvdQWi0S3qUQ0ak88QfIYsnc",
        "id": "LCC.CjgKDQoLQkNPYjhjUzM1MkkqJwoYVUNIQi13bVNTMzhWZjNOdTdVSkFLb0h3EgtCQ09iOGNTMzUySRIcChpDTWl0eXM2bGhfc0NGUklHZlFvZDZGb1BCQQ",
        "snippet": {
            "type": "textMessageEvent",
            "liveChatId": "Cg0KC0JDT2I4Y1MzNTJJKicKGFVDSEItd21TUzM4VmYzTnU3VUpBS29IdxILQkNPYjhjUzM1Mkk",
            "authorChannelId": "UCHB-wmSS38Vf3Nu7UJAKoHw",
            "publishedAt": "2022-10-30T06:10:42.827441+00:00",
            "hasDisplayContent": true,
            "displayMessage": "自分用なのでみてもおもしろくないです！！！！！！！！！",
            "textMessageDetails": {
            "messageText": "自分用なのでみてもおもしろくないです！！！！！！！！！"
            }
        },
        "authorDetails": {
            "channelId": "UCHB-wmSS38Vf3Nu7UJAKoHw",
            "channelUrl": "http://www.youtube.com/channel/UCHB-wmSS38Vf3Nu7UJAKoHw",
            "displayName": "裏飯食おばけだぞ〜",
            "profileImageUrl": "https://yt3.ggpht.com/cb4lIUm35zR2Dq8R5LihQwJsh0dxvjwzsMS8GrJLBwW36M_RiQQIEHUh1cpcOh3rQvVI3TS3pQ=s88-c-k-c0x00ffffff-no-rj",
            "isVerified": false,
            "isChatOwner": true,
            "isChatSponsor": false,
            "isChatModerator": false
        }
        },
        {
        "kind": "youtube#liveChatMessage",
        "etag": "QTqOLcH0y6IoxjJUecn6yjVC99c",
        "id": "LCC.CjgKDQoLQkNPYjhjUzM1MkkqJwoYVUNIQi13bVNTMzhWZjNOdTdVSkFLb0h3EgtCQ09iOGNTMzUySRI6ChpDTGJYeTd1bWhfc0NGWFkzclFZZENQRUhmdxIcQ01qQjA2Nm1oX3NDRmVQV1RBSWRaR0VGYnctMA",
        "snippet": {
            "type": "textMessageEvent",
            "liveChatId": "Cg0KC0JDT2I4Y1MzNTJJKicKGFVDSEItd21TUzM4VmYzTnU3VUpBS29IdxILQkNPYjhjUzM1Mkk",
            "authorChannelId": "UC7Vao7STcx3-rCTGibSBxmg",
            "publishedAt": "2022-10-30T06:14:31.444808+00:00",
            "hasDisplayContent": true,
            "displayMessage": ":elbowcough:",
            "textMessageDetails": {
            "messageText": ":elbowcough:"
            }
        },
        "authorDetails": {
            "channelId": "UC7Vao7STcx3-rCTGibSBxmg",
            "channelUrl": "http://www.youtube.com/channel/UC7Vao7STcx3-rCTGibSBxmg",
            "displayName": "ゆあいだママガール。",
            "profileImageUrl": "https://yt3.ggpht.com/tiYS8d3xpLH4691Ec2agXqulQeHgM394Px8eDTd8os6CBJp7w2xI0St8UC93kf3HuVuPTKGWOg=s88-c-k-c0x00ffffff-no-rj",
            "isVerified": false,
            "isChatOwner": false,
            "isChatSponsor": false,
            "isChatModerator": false
        }
        },
        {
        "kind": "youtube#liveChatMessage",
        "etag": "r52v68h4JtwcXvYsYuA4Or18kVE",
        "id": "LCC.CjgKDQoLQkNPYjhjUzM1MkkqJwoYVUNIQi13bVNTMzhWZjNOdTdVSkFLb0h3EgtCQ09iOGNTMzUySRI6ChpDTHp0dl82bWhfc0NGZVlNclFZZEczY01NQRIcQ09qNXo1MmtoX3NDRlRsT0R3SWR6YVlGSlEtMg",
        "snippet": {
            "type": "textMessageEvent",
            "liveChatId": "Cg0KC0JDT2I4Y1MzNTJJKicKGFVDSEItd21TUzM4VmYzTnU3VUpBS29IdxILQkNPYjhjUzM1Mkk",
            "authorChannelId": "UCRNn0QpuRX8RHqTX_BmvEMg",
            "publishedAt": "2022-10-30T06:16:51.744388+00:00",
            "hasDisplayContent": true,
            "displayMessage": ":elbowcough:",
            "textMessageDetails": {
            "messageText": ":elbowcough:"
            }
        },
        "authorDetails": {
            "channelId": "UCRNn0QpuRX8RHqTX_BmvEMg",
            "channelUrl": "http://www.youtube.com/channel/UCRNn0QpuRX8RHqTX_BmvEMg",
            "displayName": "さぶりみなるおばけ",
            "profileImageUrl": "https://yt3.ggpht.com/IKZ5my1_qxtCJ2_7pwWMSh2ldSvj0TTYr41o7J-EL8VeLT7KKMPFE5jLQM8lwOh0yLcVcTjgkg=s88-c-k-c0x00ffffff-no-rj",
            "isVerified": false,
            "isChatOwner": false,
            "isChatSponsor": false,
            "isChatModerator": false
        }
        },
        {
        "kind": "youtube#liveChatMessage",
        "etag": "QoNqoIbNr5cWNRpAZMoT_xlAZCE",
        "id": "LCC.CjgKDQoLQkNPYjhjUzM1MkkqJwoYVUNIQi13bVNTMzhWZjNOdTdVSkFLb0h3EgtCQ09iOGNTMzUySRI6ChpDTmpnc3VDbmhfc0NGUk1NZlFvZHJoUUhJURIcQ01LUW9zU25oX3NDRmZWRUR3SWRneFVPSGctMA",
        "snippet": {
            "type": "textMessageEvent",
            "liveChatId": "Cg0KC0JDT2I4Y1MzNTJJKicKGFVDSEItd21TUzM4VmYzTnU3VUpBS29IdxILQkNPYjhjUzM1Mkk",
            "authorChannelId": "UC7Vao7STcx3-rCTGibSBxmg",
            "publishedAt": "2022-10-30T06:20:17.067526+00:00",
            "hasDisplayContent": true,
    100  2037  100  2037    0     0   9974      0 --:--:-- --:--:-- --:--:-- 10034▒▒けどなんかカッコいい…！",
            "textMessageDetails": {
            "messageText": "何やってるか全くわからんけどなんかカッコいい…！"
            }
        },
        "authorDetails": {
            "channelId": "UC7Vao7STcx3-rCTGibSBxmg",
            "channelUrl": "http://www.youtube.com/channel/UC7Vao7STcx3-rCTGibSBxmg",
            "displayName": "ゆあいだママガール。",
            "profileImageUrl": "https://yt3.ggpht.com/tiYS8d3xpLH4691Ec2agXqulQeHgM394Px8eDTd8os6CBJp7w2xI0St8UC93kf3HuVuPTKGWOg=s88-c-k-c0x00ffffff-no-rj",
            "isVerified": false,
            "isChatOwner": false,
            "isChatSponsor": false,
            "isChatModerator": false
        }
        },
        {
        "kind": "youtube#liveChatMessage",
        "etag": "aAOpDkdyobqKN0V-gbnjx2Lz8OA",
        "id": "LCC.CjgKDQoLQkNPYjhjUzM1MkkqJwoYVUNIQi13bVNTMzhWZjNOdTdVSkFLb0h3EgtCQ09iOGNTMzUySRIcChpDTUhIa2FTb2hfc0NGYW9IclFZZEpVNEFZZw",
        "snippet": {
            "type": "textMessageEvent",
            "liveChatId": "Cg0KC0JDT2I4Y1MzNTJJKicKGFVDSEItd21TUzM4VmYzTnU3VUpBS29IdxILQkNPYjhjUzM1Mkk",
            "authorChannelId": "UCHB-wmSS38Vf3Nu7UJAKoHw",
            "publishedAt": "2022-10-30T06:22:39.114238+00:00",
            "hasDisplayContent": true,
            "displayMessage": "・。・v",
            "textMessageDetails": {
            "messageText": "・。・v"
            }
        },
        "authorDetails": {
            "channelId": "UCHB-wmSS38Vf3Nu7UJAKoHw",
            "channelUrl": "http://www.youtube.com/channel/UCHB-wmSS38Vf3Nu7UJAKoHw",
            "displayName": "裏飯食おばけだぞ〜",
            "profileImageUrl": "https://yt3.ggpht.com/cb4lIUm35zR2Dq8R5LihQwJsh0dxvjwzsMS8GrJLBwW36M_RiQQIEHUh1cpcOh3rQvVI3TS3pQ=s88-c-k-c0x00ffffff-no-rj",
            "isVerified": false,
            "isChatOwner": true,
            "isChatSponsor": false,
            "isChatModerator": false
        }
        },
        {
        "kind": "youtube#liveChatMessage",
        "etag": "aSKkv_P4z1SG6CotP04uoM_yc5Q",
        "id": "LCC.CjgKDQoLQkNPYjhjUzM1MkkqJwoYVUNIQi13bVNTMzhWZjNOdTdVSkFLb0h3EgtCQ09iOGNTMzUySRI6ChpDTGJ2cGJHb2hfc0NGZEFEclFZZFcwNEc4dxIcQ01LUW9zU25oX3NDRmZWRUR3SWRneFVPSGctMQ",
        "snippet": {
            "type": "textMessageEvent",
            "liveChatId": "Cg0KC0JDT2I4Y1MzNTJJKicKGFVDSEItd21TUzM4VmYzTnU3VUpBS29IdxILQkNPYjhjUzM1Mkk",
            "authorChannelId": "UC7Vao7STcx3-rCTGibSBxmg",
            "publishedAt": "2022-10-30T06:23:06.729669+00:00",
            "hasDisplayContent": true,
            "displayMessage": ":yougotthis:",
            "textMessageDetails": {
            "messageText": ":yougotthis:"
            }
        },
        "authorDetails": {
            "channelId": "UC7Vao7STcx3-rCTGibSBxmg",
            "channelUrl": "http://www.youtube.com/channel/UC7Vao7STcx3-rCTGibSBxmg",
            "displayName": "ゆあいだママガール。",
            "profileImageUrl": "https://yt3.ggpht.com/tiYS8d3xpLH4691Ec2agXqulQeHgM394Px8eDTd8os6CBJp7w2xI0St8UC93kf3HuVuPTKGWOg=s88-c-k-c0x00ffffff-no-rj",
            "isVerified": false,
            "isChatOwner": false,
            "isChatSponsor": false,
            "isChatModerator": false
        }
        }
    ]
    }

    ```

    </details>

1. 2回目以降はresponseのnextPageTokenをrequestのpageTokenとすれば差分がとれる

    request
    ```
    curl 'https://youtube.googleapis.com/youtube/v3/liveChat/messages?liveChatId=Cg0KC0JDT2I4Y1MzNTJJKicKGFVDSEItd21TUzM4VmYzTnU3VUpBS29IdxILQkNPYjhjUzM1Mkk&part=snippet&pageToken=GMWRqbGoh_sCIO3I7MCxh_sC&hl=ja&key=AIzaSyARx_t9oc9ILXrLsQGpOExLupHWg2rDtGQ'
    ```

    <details>
    <summary>response</summary>

    ```
    "kind": "youtube#liveChatMessageListResponse",
    "etag": "-FGPoqvMlmknzhuTMrvsW8Zpl7M",
    "pollingIntervalMillis": 2020,
    "pageInfo": {
        "totalResults": 3,
        "resultsPerPage": 3
    },
    "nextPageToken": "GPKO09-zh_sCIJv8qfuzh_sC",
    "items": [
        {
        "kind": "youtube#liveChatMessage",
        "etag": "8XuZ6kIDO9YMLefXyOyaOcCJEY4",
        "id": "LCC.CjgKDQoLQkNPYjhjUzM1MkkqJwoYVUNIQi13bVNTMzhWZjNOdTdVSkFLb0h3EgtCQ09iOGNTMzUySRI6ChpDSV9Oazkyemhfc0NGUkF1clFZZFppa0NkQRIcQ09qNXo1MmtoX3NDRlRsT0R3SWR6YVlGSlEtMw",
        "snippet": {
            "type": "textMessageEvent",
            "liveChatId": "Cg0KC0JDT2I4Y1MzNTJJKicKGFVDSEItd21TUzM4VmYzTnU3VUpBS29IdxILQkNPYjhjUzM1Mkk",
            "authorChannelId": "UCRNn0QpuRX8RHqTX_BmvEMg",
            "publishedAt": "2022-10-30T07:13:51.484076+00:00",
            "hasDisplayContent": true,
            "displayMessage": "あいうえお",
            "textMessageDetails": {
            "messageText": "あいうえお"
            }
        }
        },
        {
        "kind": "youtube#liveChatMessage",
        "etag": "E90SGAH0LCsVbjZ_QSiT5NLRM2U",
        "id": "LCC.CjgKDQoLQkNPYjhjUzM1MkkqJwoYVUNIQi13bVNTMzhWZjNOdTdVSkFLb0h3EgtCQ09iOGNTMzUySRI6ChpDS3JkMU42emhfc0NGZWtQclFZZG5XQUpvQRIcQ09qNXo1MmtoX3NDRlRsT0R3SWR6YVlGSlEtNA",
        "snippet": {
            "type": "textMessageEvent",
            "liveChatId": "Cg0KC0JDT2I4Y1MzNTJJKicKGFVDSEItd21TUzM4VmYzTnU3VUpBS29IdxILQkNPYjhjUzM1Mkk",
            "authorChannelId": "UCRNn0QpuRX8RHqTX_BmvEMg",
            "publishedAt": "2022-10-30T07:13:54.642473+00:00",
            "hasDisplayContent": true,
            "displayMessage": "やっほー",
            "textMessageDetails": {
            "messageText": "やっほー"
            }
        }
        },
        {
        "kind": "youtube#liveChatMessage",
        "etag": "4LypAsw29EA6iGfwAWSb2NFtTcI",
        "id": "LCC.CjgKDQoLQkNPYjhjUzM1MkkqJwoYVUNIQi13bVNTMzhWZjNOdTdVSkFLb0h3EgtCQ09iOGNTMzUySRI6ChpDTXFJMGQtemhfc0NGYXNIclFZZGhhUUxpURIcQ09qNXo1MmtoX3NDRlRsT0R3SWR6YVlGSlEtNQ",
        "snippet": {
            "type": "textMessageEvent",
            "liveChatId": "Cg0KC0JDT2I4Y1MzNTJJKicKGFVDSEItd21TUzM4VmYzTnU3VUpBS29IdxILQkNPYjhjUzM1Mkk",
            "authorChannelId": "UCRNn0QpuRX8RHqTX_BmvEMg",
            "publishedAt": "2022-10-30T07:13:56.676466+00:00",
            "hasDisplayContent": true,
            "displayMessage": "わーい",
            "textMessageDetails": {
            "messageText": "わーい"
            }
        }
        }
    ]
    }

    ```

    </details>

### quotaについて

APIコールごとにquotaなるものが消費される



